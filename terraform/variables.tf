variable "subscription_id" {
  type = string
  default = "XXXXXX"
  description = "Azure Subscription ID"
}

variable "tennant_id" {
  type = string
  default = "XXXXXX"
  description = "Azure Tennant ID"
}

variable "client_id" {
  type = string
  default = "XXXXXX"
}

variable "client_secret" {
  type = string
}

variable "agent_count" {
  type = number
  default = 2
}

variable "ssh_public_key" {
  type = string
  default = "files/ssh/igs.pub"
}

variable "dns_prefix" {
  type = string
  default = "igs-test"
}

variable cluster_name {
  type = string
  default = "igs-test"
}

variable resource_group_name {
  type = string
  default = "igs-test-rg"
}

variable location {
  type = string
  default = "westeurope"
}

variable network_plugin {
  type = string
  default = "kubenet"
}

variable kubernetes_version {
  type = string
  default = "1.17.3"
}
