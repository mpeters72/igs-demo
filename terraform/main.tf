provider "azurerm" {
  version = "~> 1.27"

  subscription_id = var.subscription_id
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tennant_id
}

provider "azuread" {
  version = "~>0.3"

  subscription_id = var.subscription_id
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tennant_id
}
