#!/bin/bash

# Namespace for the ingress controller. 
# Ensure this matches the namespace where you are going to deploy your pods
NAMESPACE=igs

# Create the namespace
kubectl create namespace $NAMESPACE

# Add the nginx ingress helm repo
helm repo add stable https://kubernetes-charts.storage.googleapis.com/

# Deploy the ingress controller
helm install nginx stable/nginx-ingress \
    --namespace $NAMESPACE \
    --set controller.replicaCount=2 \
    --set controller.nodeSelector."beta\.kubernetes\.io/os"=linux \
    --set defaultBackend.nodeSelector."beta\.kubernetes\.io/os"=linux
