# IGS Demo Application

This repo contains scripts to create a Kubernetes cluster and deploy the IGS demo application via helm.

A running version of the application is currently deployed to http://igs-test.westeurope.cloudapp.azure.com, deployed using the terraform and helm detailed scripts below.

## Instructions

### Deploy Via Manifests

I have included YAML files in the `manifests` sub directory to deploy the application to an existing cluster as follows. From within the `manifests` directory and assuming kubectl is configured to point to your cluster:

```
# Deploy the namespace
kubectl apply -f ns.yml

# Deploy the pods
kubectl apply -f deployment.yml

# Deploy the services
kubectl apply -f services.yml

# Deploy the ingress (this assumes you have a suitable controller installed already 
# and you potentially need to change the value of host in the ingress.yml file to 
# point to your cluster)
kubectl apply -f ingress.yml

```

For a full deployment using helm and including installing a cluster to Azure AKS, see the process below.

### Deploy a Kubernetes Cluster to Azure

Assuming you have [terraform](https://learn.hashicorp.com/terraform/getting-started/install.html
"Install Terraform") installed on the machine where you have checked out this
repo:

1. Change directory to the `terraform` directory within this repo.

2. Open the `variables.tf` file in an editor and change the client_id, tennant_id
   and subscription id values to match those of your Azure account.

3. Replace the file, `terraform/files/ssh/igs.pub` with your public ssh key
   which is used to log in to your k8s worker nodes if required (alternatively
   change the value of `ssh_public_key` in `variables.tf` to point to your
   local public key) 

4. Run `terraform apply` You will be prompted for your client secret and if
   successful the terraform output should end with something like: 
   ``` 
   Plan: 5 to add, 0 to change, 0 to destroy.


   Warning: "agent_pool_profile": [DEPRECATED] This has been replaced by `default_node_pool` and will be removed in version 2.0 of the AzureRM Provider

   on cluster.tf line 21, in resource "azurerm_kubernetes_cluster" "aks":
   21: resource "azurerm_kubernetes_cluster" "aks" {


   Do you want to perform these actions?
   Terraform will perform the actions described above.
   Only 'yes' will be accepted to approve.

   Enter a value:

   ``` 

5. Enter "yes", at which point terraform should build your k8s cluster 

6. Once complete, you should configure `kubectl` to connect the cluster by
   running 

   ```
   echo "$(terraform output kube_config)" > ~/.kube/azurek8s export
   export KUBECONFIG=~/.kube/azurek8s 
   ``` 

   Test the health of your cluster by running `kubectl get nodes`

### Deploy The NGINX Ingress Controller

I have configured the deployment to use the NGINX ingress controller. In the
`scripts` subdirectory of the repo I have included a script to deploy the
controller. From within this folder, assuming you have `helm` installed, run
`./ingress.sh`

Once that has run, run `kubectl --namespace igs get services -o wide -w nginx-nginx-ingress-controller` and wait for the public IP to appear and take a note of it.

The IP address noted above is where you need to point your DNS if you wish to access the demo via DNS. You can create a record via the Azure console by browsing the "Public IP adresses" section of the console and, after selecting the appropriate address, choosing Settings -> Configuration and adding a "DNS name label", for example "igs-demo", which will create a DNS record of "igs-demo.westeurope.cloudapp.azure.com" pointing to your cluster

### Deploy the App via Helm

Assuming helm is installed, you can deploy the application to your cluster as follows:

1. First open the file `igs_chart/values.yaml`, the defaults should be mostly OK, however if you have a DNS record pointing to your cluster you should amend the value of `host` to match.

2. Run `helm install igs-demo igs_chart` from the root of the repo to deploy

3. Check the deployment by running `kubectl get pods -n igs -w`. Once all pods are running the application should be available on the DNS name you configured.
